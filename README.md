This is a collection of documents from public authorities that can be used as
references by data subjects to have their rights under the GDPR acknowledged.

It is generated taking advantage of `biblatex`’s features, mainly
`printbibliography`'s `keyword` filter.  Entries are registered in a `.bib`
file, therefore the collection can be processed programmatically.

The documents are collected here and there, from various and different sources,
with no established frequency.  Therefore, the information may not be updated
and there is no warranty that it is correct or up to date.  However, the
authors are entities recognized or established by law, with specific competence
about the protection of personal data or European law.
